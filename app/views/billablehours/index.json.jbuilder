json.array!(@billablehours) do |billablehour|
  json.extract! billablehour, :id, :project_id, :business_id, :user_id, :hours, :start_time, :end_time, :timestamp
  json.url billablehour_url(billablehour, format: :json)
end
