# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150615223841) do

  create_table "billablehours", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "business_id"
    t.integer  "user_id"
    t.float    "hours"
    t.date     "start_time"
    t.date     "end_time"
    t.datetime "timestamp"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "businesses", force: :cascade do |t|
    t.string   "business_name"
    t.string   "phone"
    t.text     "address"
    t.string   "email"
    t.datetime "timestamp"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string   "project_name"
    t.integer  "user_id"
    t.integer  "business_id"
    t.float    "budget"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "timestamp"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.string   "password"
    t.string   "role"
    t.string   "email"
    t.string   "phone"
    t.text     "address"
    t.float    "hourlyrate"
    t.date     "hiredate"
    t.string   "photo"
    t.datetime "timestamp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
