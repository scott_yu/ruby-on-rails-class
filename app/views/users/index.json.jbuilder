json.array!(@users) do |user|
  json.extract! user, :id, :first_name, :last_name, :username, :password, :role, :email, :phone, :address, :hourlyrate, :hiredate, :photo, :timestamp
  json.url user_url(user, format: :json)
end
