class WelcomeController < ApplicationController
  def index
  	if(session[:user_id])
  		@user = User.find_by_id(session[:user_id])
  		redirect_to ("/login/signin?username=#{@user[:username]}")
  	end
  end

  def logout
  	reset_session
  	redirect_to ("/")
  end
end
