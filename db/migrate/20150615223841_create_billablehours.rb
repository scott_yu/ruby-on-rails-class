class CreateBillablehours < ActiveRecord::Migration
  def change
    create_table :billablehours do |t|
      t.integer :project_id
      t.integer :business_id
      t.integer :user_id
      t.float :hours
      t.date :start_time
      t.date :end_time
      t.timestamp :timestamp

      t.timestamps null: false
    end
  end
end
