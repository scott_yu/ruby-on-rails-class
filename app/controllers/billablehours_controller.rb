class BillablehoursController < ApplicationController
  before_action :set_billablehour, only: [:show, :edit, :update, :destroy]
  # GET /billablehours
  # GET /billablehours.json
  def index
    @project_id = params[:project_id]
    @user = User.find_by_id(session[:user_id])
    if(@user.id && @user.id !="5") 
      @billablehours = Billablehour.joins("LEFT JOIN projects b on b.id = billablehours.project_id").where("billablehours.user_id=#{@user.id} AND billablehours.project_id = #{@project_id} AND billablehours.paid IS NULL").all.select("billablehours.*, b.project_name")
    else
      @billablehours = Billablehour.all
    end
  end

  # GET /billablehours/1
  # GET /billablehours/1.json
  def show
  end

  # GET /billablehours/new
  def new
    @billablehour = Billablehour.new
    @user = User.find_by_id(session[:user_id])
    @projects = Project.all.where("user_id=#{@user.id}").order(project_name: :asc)  #Records those has a content
    @project_id = params[:project_id]
  end

  # GET /billablehours/1/edit
  def edit
    @user = User.find_by_id(session[:user_id])
    @projects = Project.all.where("user_id=#{@user.id}").order(project_name: :asc)  #Records those has a content
  end

  # POST /billablehours
  # POST /billablehours.json
  def create
    @billablehour = Billablehour.new(billablehour_params)

    respond_to do |format|
      if @billablehour.save
        format.html { redirect_to "/" }
        format.json { render :show, status: :created, location: @billablehour }
      else
        format.html { render :new }
        format.json { render json: @billablehour.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /billablehours/1
  # PATCH/PUT /billablehours/1.json
  def update
    respond_to do |format|
      if @billablehour.update(billablehour_params)
        format.html { redirect_to @billablehour, notice: 'Billablehour was successfully updated.' }
        format.json { render :show, status: :ok, location: @billablehour }
      else
        format.html { render :edit }
        format.json { render json: @billablehour.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /billablehours/1
  # DELETE /billablehours/1.json
  def destroy
    @billablehour.destroy
    respond_to do |format|
      format.html { redirect_to billablehours_url, notice: 'Billablehour was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_billablehour
      @billablehour = Billablehour.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def billablehour_params
      params.require(:billablehour).permit(:project_id, :business_id, :user_id, :hours, :start_time, :end_time, :timestamp)
    end
end
