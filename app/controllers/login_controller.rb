class LoginController < ApplicationController
  def signin
    @user = User.find_by_username(params[:username])
    #@projects = Project.where("user_id = ?",session[:user_id])
    #@billablehours = Billablehour.joins(:projects).where("user_id = ?",session[:user_id])
    @totalpay = ActiveRecord::Base.connection.select_one("SELECT sum(hours) as totalhours from billablehours WHERE user_id = #{@user.id} AND paid = '1' AND date('now','start of year')");
    if(!@totalpay['totalhours']) 
      @totalpay['totalhours'] = 0
    end
    @projects = ActiveRecord::Base.connection.select_all("select a.id,a.project_name, sum(b.hours) as totalhours from projects a LEFT JOIN billablehours b ON b.project_id = a.id where a.user_id=#{@user.id} AND paid IS NULL GROUP by project_name")
    session[:user_id] = @user.id
    respond_to do |format|
      format.html # show.html.erb
    end#redirect_to ("/users/" + @username)
  end
end
