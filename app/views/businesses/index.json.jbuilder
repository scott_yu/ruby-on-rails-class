json.array!(@businesses) do |business|
  json.extract! business, :id, :business_name, :phone, :address, :email, :timestamp
  json.url business_url(business, format: :json)
end
