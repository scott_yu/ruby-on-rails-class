class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :project_name
      t.integer :user_id
      t.integer :business_id
      t.float :budget
      t.datetime :start_time
      t.datetime :end_time
      t.timestamp :timestamp

      t.timestamps null: false
    end
  end
end
