json.array!(@projects) do |project|
  json.extract! project, :id, :project_name, :user_id, :business_id, :budget, :start_time, :end_time, :timestamp
  json.url project_url(project, format: :json)
end
