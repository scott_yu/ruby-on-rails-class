class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.string :business_name
      t.string :phone
      t.text :address
      t.string :email
      t.timestamp :timestamp

      t.timestamps null: false
    end
  end
end
