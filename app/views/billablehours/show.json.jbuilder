json.extract! @billablehour, :id, :project_id, :business_id, :user_id, :hours, :start_time, :end_time, :timestamp, :created_at, :updated_at
