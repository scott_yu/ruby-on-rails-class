class MailController < ApplicationController
  def index
  	@user = User.find_by_id(params[:user_id])
  	@project = Project.find_by_id(params[:project_id])
  	@business = ActiveRecord::Base.connection.select_one("select * from businesses where id = (select business_id from projects where id = #{params[:project_id]} )")
  	Pony.mail(:to => @business['email'], :subject => "Invoice for work", :body => "#{@user.email} requests payment for #{@project.project_name}")
  end
end
