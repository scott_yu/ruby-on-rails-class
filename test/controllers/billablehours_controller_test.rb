require 'test_helper'

class BillablehoursControllerTest < ActionController::TestCase
  setup do
    @billablehour = billablehours(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:billablehours)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create billablehour" do
    assert_difference('Billablehour.count') do
      post :create, billablehour: { business_id: @billablehour.business_id, end_time: @billablehour.end_time, hours: @billablehour.hours, project_id: @billablehour.project_id, start_time: @billablehour.start_time, timestamp: @billablehour.timestamp, user_id: @billablehour.user_id }
    end

    assert_redirected_to billablehour_path(assigns(:billablehour))
  end

  test "should show billablehour" do
    get :show, id: @billablehour
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @billablehour
    assert_response :success
  end

  test "should update billablehour" do
    patch :update, id: @billablehour, billablehour: { business_id: @billablehour.business_id, end_time: @billablehour.end_time, hours: @billablehour.hours, project_id: @billablehour.project_id, start_time: @billablehour.start_time, timestamp: @billablehour.timestamp, user_id: @billablehour.user_id }
    assert_redirected_to billablehour_path(assigns(:billablehour))
  end

  test "should destroy billablehour" do
    assert_difference('Billablehour.count', -1) do
      delete :destroy, id: @billablehour
    end

    assert_redirected_to billablehours_path
  end
end
